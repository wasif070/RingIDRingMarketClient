/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.market;

import java.net.InetSocketAddress;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.net.SocketAddress;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.SocketConnector;

/**
 *
 * @author Wasif
 */
public class MarketClient {

    private ITCPReceiver tcpReceiver;
    private final int timeOut = 10 * 1000;
    public boolean stop = false;
    public final int BUFFER_SIZE = 8192;
    private SocketAddress socketAddress;
    private ExecutorService executor = Executors.newFixedThreadPool(10);
    private ConnectionPool connectionPool;
    private Callable callableSocketConnector = (Callable) () -> connect();
    private static final Logger LOG = Logger.getLogger(MarketClient.class);
    private final RequestProcessor requestProcessor;

    public MarketClient(String hostname, int port, ITCPReceiver receiver) {
        this.tcpReceiver = receiver;
        this.socketAddress = new InetSocketAddress(hostname, port);
        this.connectionPool = new ConnectionPool();
        this.executeConnect();
        this.requestProcessor = new RequestProcessor(this, connectionPool);
        this.requestProcessor.start();

    }

    public void executeConnect() {
        try {
            executor.execute(run);
        } catch (Exception e) {
            LOG.error("MarketClient: executerSubmit-->", e.getCause());
        }
    }
    private Runnable run = () -> {
        try {
            Object o = callableSocketConnector.call();
        } catch (Exception e) {
            LOG.error("MarketClient->run-->", e.getCause());
        }
    };

    private boolean connect() {
        IoSession session = null;
        SocketConnector connector = new NioSocketConnector(Runtime.getRuntime().availableProcessors() + 1);
        connector.setConnectTimeoutMillis(this.timeOut);
        connector.getSessionConfig().setReuseAddress(true);
        connector.getSessionConfig().setSendBufferSize(BUFFER_SIZE);
        ((SocketSessionConfig) connector.getSessionConfig()).setReuseAddress(true);
        ((SocketSessionConfig) connector.getSessionConfig()).setTcpNoDelay(true);
        connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
        connector.setHandler(new SessionHandler(this, connectionPool, tcpReceiver, connector));
        ConnectFuture connectFuture;

        while (!stop && (session == null || !session.isConnected())) {
            try {
                connectFuture = connector.connect(socketAddress).awaitUninterruptibly();
                session = connectFuture.getSession();
            } catch (Exception e) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    LOG.error("MarketClient->Connect InterruptedException ex-->" + ex.getMessage());
                }
                LOG.error("MarketClient->Connecting-->" + e.getMessage());
            }
        }
        //this.connectionPool.getConnectorMap().put(session, connector);
        return true;
    }

    public void sendRequest(byte[] request) {
        this.connectionPool.getRequestQueue().add(request);
    }

    public void closeAllConnections() {
        LOG.debug("MarketClient: before closeAllConnections--> connector closed!");
        this.stop = true;
        this.requestProcessor.stopService();
        this.connectionPool.closeAllConnections();
        this.executor.shutdown();
        LOG.debug("MarketClient: after closeAllConnections--> connector closed!");
    }

    public void makeFalseResponse(byte[] request) {
        try {
            byte[] market_bytes;
            JsonParser parser = new JsonParser();
            JsonObject json = parser.parse(new String(request)).getAsJsonObject();

            JsonObject obj = new JsonObject();
            obj.addProperty("pckId", json.get("pckId").getAsString());
            obj.addProperty("sId", json.get("sId").getAsString());
            obj.addProperty("actn", json.get("actn").getAsInt());
            obj.addProperty("sucs", Boolean.FALSE);
            obj.addProperty("rc", 13);
            obj.addProperty("mg", "Market Server not responding!");

            byte[] response = obj.toString().getBytes();
            String src_pck_id = json.get("pckId").getAsString();
            market_bytes = new byte[response.length + src_pck_id.length() + 1];
            market_bytes[0] = (byte) src_pck_id.length();
            System.arraycopy(src_pck_id.getBytes(), 0, market_bytes, 1, src_pck_id.length());
            System.arraycopy(response, 0, market_bytes, src_pck_id.length() + 1, response.length);
            this.tcpReceiver.receive(response);
            LOG.debug("MarketClient->makeFalseResponse: error response to client--> " + obj.toString());
        } catch (JsonSyntaxException e) {
            LOG.error("MarketClient->makeFalseResponse: ex-->", e);
        }
    }

//    public static void main(String[] args) {
//        String a = "{\"sucs\":true,\"prodLst\":[{\"id\":\"11e7-8b1a-1d4f8250-ac78-07759a0bcd66\",\"nm\":\"KEEN Kean Newport H two sandals NEWPORT H2 sneakers shoes [men's Lady's]\",\"isExclsv\":false,\"lat\":23.5448,\"lon\":89.1539,\"sts\":1,\"nPrc\":269.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/1876771520346330454.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934148901650,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970022\"},{\"id\":\"11e7-8b1a-1da4f550-ac78-07759a0bcd66\",\"nm\":\"spruce / spruce canvas schoolluc / flap top square Backpack Rucksack backpack fashion school backpack know good mothersluc bag ladies backpack diaper bag adult brand backpack\",\"isExclsv\":false,\"lat\":23.5448,\"lon\":89.1539,\"sts\":1,\"nPrc\":213.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/9439641520346329708.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934715281365,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970504\"},{\"id\":\"11e7-8b1a-24a7d980-ac78-07759a0bcd66\",\"nm\":\"e north face bags THE NORTH FACE T0CHK4 JK3 BOREALIS Backpack Backpack TNF BLACKIT\",\"isExclsv\":false,\"lat\":23.4873,\"lon\":89.42,\"sts\":1,\"nPrc\":683.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/8330771520346377295.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934763422923,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970360\"},{\"id\":\"11e7-8b1a-24f62090-ac78-07759a0bcd66\",\"nm\":\"idas originals Adidas Stan Smith STAN SMITH W BB5155 BB5156 black and white gold and silver originals [men's Lady's]ei\",\"isExclsv\":false,\"lat\":23.1641,\"lon\":90.1897,\"sts\":1,\"nPrc\":752.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/5612681520346257265.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8933608983506,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294969792\"},{\"id\":\"11e7-8b1a-267dd200-ac78-07759a0bcd66\",\"nm\":\"ach tote bag outlet COACH F58846 IMCHK whiteor\",\"isExclsv\":false,\"lat\":23.4683,\"lon\":91.1788,\"sts\":1,\"nPrc\":245.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/9866371520346327612.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934459192655,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970480\"},{\"id\":\"11e7-8b1a-276795c0-ac78-07759a0bcd66\",\"nm\":\"Full lah long wallet FURLA 856529 PN84 B30 BL7 blue\",\"isExclsv\":false,\"lat\":23.7115,\"lon\":90.4111,\"sts\":1,\"nPrc\":750.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/5673751520346328513.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934712014427,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970483\"},{\"id\":\"11e7-8b1a-27acb510-ac78-07759a0bcd66\",\"nm\":\"MINNETONKA / Minnetonka Miniture Moc Key Rings and miniature moccasin Keyring\",\"isExclsv\":false,\"lat\":23.9571,\"lon\":91.1119,\"sts\":1,\"nPrc\":884.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/1821091520346296271.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8933631726336,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294969839\"},{\"id\":\"11e7-8b1a-28b54b70-ac78-07759a0bcd66\",\"nm\":\"Select Manual store limited model PUMA PUMA Kauto star GOLD 361051 leather shoes sneakers white [men and women] [return &amp; Exchange OK]\",\"isExclsv\":true,\"lat\":26.3411,\"lon\":88.5542,\"sts\":1,\"nPrc\":228.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/3068021520346369102.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934738897916,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970304\"},{\"id\":\"11e7-8b1a-290b0c90-ac78-07759a0bcd66\",\"nm\":\"anello / Anello flap top square daypack mini / rucksack backpack fashion backpack school backpack know good mothersluc Luc lease Luc Womens backpack diaper bag adult brand backpack\",\"isExclsv\":false,\"lat\":23.1664,\"lon\":89.2081,\"sts\":1,\"nPrc\":180.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/9529011520346299719.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8933636255754,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294969861\"},{\"id\":\"11e7-8b1a-2924d620-ac78-07759a0bcd66\",\"nm\":\"NIKE Kie Ney AMAX prime AIR MAX PRIME 876068 sneakers shoes middle [men's Lady's]\",\"isExclsv\":false,\"lat\":22.6859,\"lon\":90.6482,\"sts\":1,\"nPrc\":619.0,\"imgUrl\":\"cloud/uploadedProfile-1058/2110100310/6322331520346291950.jpg\",\"imgH\":300,\"imgW\":300,\"curCd\":\"BDT\",\"crt\":8934173986037,\"ut\":0,\"shpId\":68719476836,\"shpNm\":\"Bata\",\"cd\":\"4294970192\"}],\"pckId\":\"2122222456IYglPTCj\",\"actn\":4019}";
//        byte[] b = a.getBytes();
//        long time = System.currentTimeMillis();
//        System.out.println(new String(b));
//        System.out.println("interval-->" + (System.currentTimeMillis() - time));
//    }
}
