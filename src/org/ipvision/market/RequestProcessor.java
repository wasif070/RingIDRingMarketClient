/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.market;

import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

/**
 *
 * @author Wasif
 */
public class RequestProcessor extends Thread {

    private static final Logger LOG = Logger.getLogger(RequestProcessor.class);
    private boolean running;
    private final ConnectionPool connectionPool;
    private final MarketClient client;
    private final int MAX_CONNECTIONS = 100;
    private int CURRENT_CONNECTIONS = 0;

    public RequestProcessor(MarketClient client, ConnectionPool connectionPool) {
        this.running = true;
        this.client = client;
        this.connectionPool = connectionPool;
    }

    int count = 0;

    @Override
    public void run() {
        LinkedBlockingQueue<byte[]> requestQueue = connectionPool.getRequestQueue();

        while (running) {
            while (running && !requestQueue.isEmpty()) {

                byte[] request_bytes = requestQueue.poll();

                IoSession session = this.connectionPool.getFreeConnection();
                CURRENT_CONNECTIONS = this.connectionPool.size();

                if (session == null) {
                    //this.client.makeFalseResponse(request_bytes);
                    if (CURRENT_CONNECTIONS > 0 && CURRENT_CONNECTIONS < MAX_CONNECTIONS) {
                        this.client.executeConnect();
                    } else if (CURRENT_CONNECTIONS >= MAX_CONNECTIONS) {
                        this.connectionPool.makeABusyConnectionToFree();
                    }
                    
                    await(1000);

                    session = this.connectionPool.getFreeConnection();
                    if (session == null || !session.isConnected()) {
                        this.client.makeFalseResponse(request_bytes);
                    } else {
                        session.write(request_bytes);
                    }

                } else if (!session.isConnected()) {

                    //byte[] bytes = requestQueue.poll();
                    this.client.makeFalseResponse(request_bytes);
                    this.connectionPool.removeConnection(session);

                } else if (session.isConnected()) {

                    //byte[] bytes = requestQueue.poll();
                    session.write(request_bytes);

                }
            }
            await(100);
        }
    }

    private void await(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException iex) {
            Thread.currentThread().interrupt();
            LOG.error("Thread Exception await in RequestProcessor-->", iex);
        }
    }

    public void stopService() {
        running = false;
        this.interrupt();
    }
}
