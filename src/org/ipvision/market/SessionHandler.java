/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.market;

import com.google.gson.JsonObject;
import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.SocketConnector;
import org.apache.mina.transport.socket.SocketSessionConfig;

/**
 *
 * @author Wasif
 */
public class SessionHandler extends IoHandlerAdapter {
    
    private static final Logger log = Logger.getLogger(SessionHandler.class);
    private ITCPReceiver receiver;
    private MarketClient client;
    private ConnectionPool connectionPool;
    private SocketConnector connector;
    
    public SessionHandler(MarketClient client, ConnectionPool connectionPool, ITCPReceiver receiver, SocketConnector connector) {
        this.client = client;
        this.connectionPool = connectionPool;
        this.receiver = receiver;
        this.connector = connector;
    }
    
    @Override
    public void sessionOpened(IoSession session) {
        JsonObject json = new JsonObject();
        json.addProperty("actn", 200);
        session.write(json.toString().getBytes());
        //log.debug("SessionHandler->sessionOpened: " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
    }
    
    @Override
    public void messageReceived(IoSession session, Object message) {
        this.receiver.receive((byte[]) message);
        this.connectionPool.makeConnectionFree(session);
        log.debug("SessionHandler->messageReceived: " + new String((byte[]) message) + " from " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
    }
    
    @Override
    public void exceptionCaught(IoSession session, Throwable cause) {
        log.error("SessionHandler->exceptionCaught: ", cause);
    }
    
    @Override
    public void inputClosed(IoSession session) throws Exception {
        if (!this.client.stop) {
            this.connectionPool.removeConnection(session);
        }
    }
    
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        log.debug("SessionHandler->messageSent: " + new String((byte[]) message) + " to " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
    }
    
    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        //log.debug(MarketClient.class.getName() + "->sessionIdle: " + status.toString() + " of " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
    }
    
    @Override
    public void sessionClosed(IoSession session) throws Exception {
        log.debug("SessionHandler->sessionClosed: " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
        if (!this.client.stop) {
            this.connectionPool.removeConnection(session);
            this.client.executeConnect();
        }
    }
    
    @Override
    public void sessionCreated(IoSession session) throws Exception {
        log.debug("SessionHandler->sessionCreated: " + ((session.getRemoteAddress() != null) ? session.getRemoteAddress().toString() : session.getRemoteAddress()));
        this.connectionPool.getConnectorMap().put(session, connector);
        //this.connectionPool.makeConnectionFree(session);
        if (session.getConfig() instanceof SocketSessionConfig) {
            ((SocketSessionConfig) session.getConfig()).setReceiveBufferSize(client.BUFFER_SIZE);
        }
        session.getConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
    }
    
}
