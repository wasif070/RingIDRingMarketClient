/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.market;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.SocketConnector;

/**
 *
 * @author Wasif
 */
public class ConnectionPool {

    private final Queue<byte[]> requestQueue;
    private final Queue<IoSession> availableSessionQueue;
    private final Queue<IoSession> busySessionQueue;
    private final ConcurrentHashMap<IoSession, Long> busySessionTime;
    private final ConcurrentHashMap<IoSession, SocketConnector> connectorMap;
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private final long CONNECTION_TIMEOUT = 60 * 1000L;

    public ConnectionPool() {
        requestQueue = new LinkedBlockingQueue<>();
        availableSessionQueue = new ConcurrentLinkedQueue<>();
        busySessionQueue = new ConcurrentLinkedQueue<>();
        connectorMap = new ConcurrentHashMap<>();
        busySessionTime = new ConcurrentHashMap<>();
    }

    public LinkedBlockingQueue getRequestQueue() {
        return (LinkedBlockingQueue) requestQueue;
    }

    public ConcurrentHashMap getConnectorMap() {
        return connectorMap;
    }

    public synchronized int size() {
        LOG.debug("ConnectionPool: availableSessionQueue.size()-->" + availableSessionQueue.size() + " busySessionQueue.size()-->" + busySessionQueue.size());
        return (availableSessionQueue.size() + busySessionQueue.size());
    }

    public boolean makeABusyConnectionToFree() {
        long time = System.currentTimeMillis();
        for (IoSession session : busySessionQueue) {
            if (session != null && busySessionTime.containsKey(session)) {
                long last_time = busySessionTime.get(session);
                if (last_time > 0 && (time - last_time) > 3 * CONNECTION_TIMEOUT) {
                    if (session.isConnected()) {
                        makeConnectionFree(session);
                        return true;
                    } else {
                        removeConnection(session);
                    }
                }
            }
        }

        return false;
    }

    public void makeConnectionFree(IoSession session) {
        //LOG.debug("Before makeConnectionFree " + this.size());
        availableSessionQueue.offer(session);
        busySessionQueue.remove(session);
        busySessionTime.remove(session);
        //LOG.debug("After makeConnectionFree " + this.size());
    }

    public IoSession getFreeConnection() {
        long current_time = System.currentTimeMillis();
        IoSession session = availableSessionQueue.poll();
        if (session != null) {
            busySessionQueue.offer(session);
            busySessionTime.put(session, current_time);
        } else {
            //check busy session busy time
            if (makeABusyConnectionToFree()) {
                return getFreeConnection();
            }

        }
        return session;
    }

    public void removeConnection(IoSession session) {
        availableSessionQueue.remove(session);
        busySessionQueue.remove(session);
        busySessionTime.remove(session);
        SocketConnector connector = connectorMap.get(session);
        if (connector != null) {
            connector.dispose();
            connectorMap.remove(session);
            session.closeNow();
        }
    }

    /*public void closeAllConnections() {
        LOG.debug("ConnectionPool->before closeAllConnections " + this.size());

        for (Map.Entry<IoSession, SocketConnector> entry : connectorMap.entrySet()) {
            entry.getKey().closeNow();
            entry.getValue().dispose();
        }
        LOG.debug("ConnectionPool->after closeAllConnections " + this.size());
    }*/
    public void closeAllConnections() {
        /*LOG.debug("ConnectionPool->closeAllConnections before availableSessionQueue size-->" + availableSessionQueue.size());
        while (!availableSessionQueue.isEmpty()) {
            IoSession session = availableSessionQueue.poll();
            if (session != null) {
                SocketConnector connector = connectorMap.get(session);
                session.closeNow();
                if (connector != null) {
                    connector.dispose();
                }
            }
        }
        LOG.debug("ConnectionPool->closeAllConnections after availableSessionQueue size-->" + availableSessionQueue.size());

        LOG.debug("ConnectionPool->closeAllConnections before busySessionQueue size-->" + busySessionQueue.size());
        while (!busySessionQueue.isEmpty()) {
            IoSession session = busySessionQueue.poll();
            if (session != null) {
                SocketConnector connector = connectorMap.get(session);
                session.closeNow();
                if (connector != null) {
                    connector.dispose();
                }
            }
        }
        LOG.debug("ConnectionPool->closeAllConnections after busySessionQueue size-->" + busySessionQueue.size());*/

        LOG.debug("ConnectionPool->closeAllConnections before size-->" + this.size());
        for (Map.Entry<IoSession, SocketConnector> entry : connectorMap.entrySet()) {
            entry.getKey().closeNow();
            entry.getValue().dispose();
        }
        LOG.debug("ConnectionPool->closeAllConnections after size-->" + this.size());
    }
}
